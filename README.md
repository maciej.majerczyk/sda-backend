# Projekt django w technologie backendowe


Po pobraniu repozytorium za pomocą `git clone` należy stworzyć
 wirtualne środowisko w projekcie
 
Następnie po aktywacji środowiska w terminalu (venv widoczne po lewej stronie) 
zainstalować plik requirements.txt za pomocą
```bash
pip install -r requirements.txt
``` 

Następnie trzeba dokonać migracji bazy danych
```bash
python manage.py makemigrations
python manage.py migrate
```
Jeżeli nie wygenerowałyby się migracje dla newapp można wykonać te polecenia
```bash
python manage.py makemigrations newapp
python manage.py migrate
```

Po stworzeniu migracji należy dodać superuser'a , aby mieć dostęp do portalu admina:
```bash
python manage.py createsuperuser
```

Finalnie uruchmoić serwer za pomocą:
```bash
python manage.py runserver
```