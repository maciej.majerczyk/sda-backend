from django.urls import path

from .views import ProductListView, ProductDetailView, ProducerDetailView, ProducerListView, CategoryListView, \
    CategoryDetailView, HomeTemplateView

urlpatterns = [
    path('', HomeTemplateView.as_view(), name='home'),
    path('products/', ProductListView.as_view(), name='products'),
    path('products/<int:pk>/', ProductDetailView.as_view(), name='product_detail'),
    path('producers/', ProducerListView.as_view(), name='producers'),
    path('producers/<int:pk>/', ProducerDetailView.as_view(), name='producer_detail'),
    path('categories/', CategoryListView.as_view(), name='categories'),
    path('categories/<int:pk>/', CategoryDetailView.as_view(), name='category_detail'),

]
