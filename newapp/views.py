# Create your views here.
from django.shortcuts import render
from django.views.generic import ListView, DetailView, TemplateView

from newapp.models import Product, Category, Producer


def index(request):
    products = Product.objects.all()
    context = {'products': products}
    return render(request, 'product_list.html', context=context)


class HomeTemplateView(TemplateView):
    template_name = 'home.html'


class ProductListView(ListView):
    model = Product
    template_name = 'product_list.html'
    context_object_name = 'products'


class ProductDetailView(DetailView):
    model = Product
    template_name = 'product_detail.html'


class CategoryListView(ListView):
    model = Category
    template_name = 'category_list.html'
    context_object_name = 'categories'


class CategoryDetailView(DetailView):
    model = Category
    template_name = 'category_detail.html'


class ProducerListView(ListView):
    model = Producer
    template_name = 'producer_list.html'


class ProducerDetailView(DetailView):
    model = Producer
    template_name = 'producer_detail.html'
