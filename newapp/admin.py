from django.contrib import admin

# Register your models here.
from newapp.models import Post, Product, Category, Producer

admin.site.register(Producer)
admin.site.register(Post)
admin.site.register(Category)
admin.site.register(Product)
